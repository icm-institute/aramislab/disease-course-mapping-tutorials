Link to the Google Colab of the notebook :

https://colab.research.google.com/drive/1uZDC4LYE8tmBG83UGcZZPfWr-UUBRxgH?usp=sharing

Please open the link above and save a copy on your drive (in the File menu). This should open a copy of it which you can freely edit and run, enjoy !

You can still find the solutions in solutions folder of this repository.
